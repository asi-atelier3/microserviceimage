from flask import Flask, request
from Tags import load_image
import json

app = Flask(__name__)

@app.route("/api/tags", methods = ["POST"])
def getTags():
	request_data = request.get_json()

	img = None
	print(request_data)
	if request_data:
		if "img" in request_data:
			img = request_data["img"]	
	tags = load_image(img)
	print(tags)
	return json.dumps(tags)

if __name__ == "__main__":
	app.run(debug=True, port=8082, host="0.0.0.0")
