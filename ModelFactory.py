from Model import Model

class ModelFactory:
    __instance = None

    def get_instance(self):
        if ModelFactory.__instance is None :
            print("Load model...")
            ModelFactory.__instance = Model()
        return ModelFactory.__instance