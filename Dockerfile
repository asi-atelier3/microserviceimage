FROM python:3.7-slim

RUN  apt-get update && \
  apt install -y protobuf-compiler gcc 


COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt

RUN cd model/research/ && \
   protoc object_detection/protos/*.proto --python_out=.  && \
   cp object_detection/packages/tf2/setup.py .  && \
   python -m pip install . 

EXPOSE 8082

CMD ["python", "ImageRestCtr.py"]
