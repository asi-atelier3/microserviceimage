# MicroServiceImage

```
pip install -U "tensorflow>=2.5"
pip install --upgrade tensorflow-hub
sudo apt install -y protobuf-compiler
cd models/research/
protoc object_detection/protos/*.proto --python_out=.
cp object_detection/packages/tf2/setup.py .
python -m pip install .
```

POST on /getTags with body : 
{
	...,
	"url": "url_of_an_image",
	...
}

return JSON : ["tags1", "tags2", "tags3"] 


Build Dockerfile:

docker build -t microserviceimage .
docker run -d -p 8082:8082 --name mon-service microserviceimage