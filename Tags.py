#!/usr/bin/env python
# coding: utf-8

# ##### Copyright 2020 The TensorFlow Hub Authors.
# 
# Licensed under the Apache License, Version 2.0 (the "License");

#@title Copyright 2020 The TensorFlow Hub Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# # TensorFlow Hub Object Detection Colab
import matplotlib.pyplot as plt

import numpy as np
from six import BytesIO
from PIL import Image
from six.moves.urllib.request import urlopen

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils
from object_detection.utils import ops as utils_ops

import tensorflow as tf

from ModelFactory import ModelFactory

tf.get_logger().setLevel('ERROR')


# ## Utilities
# 
# - Helper method to load an image
# - Map of Model Name to TF Hub handle
# - List of tuples with Human Keypoints for the COCO 2017 dataset. This is needed for models with keypoints.

def load_image_into_numpy_array(path):
	"""Load an image from file into a numpy array.

	Puts image into numpy array to feed into tensorflow graph.
	Note that by convention we put it into a numpy array with shape
	(height, width, channels), where channels=3 for RGB.

	Args:
		path: the file path to the image

	Returns:
		uint8 numpy array with shape (img_height, img_width, 3)
	"""
	image = None
	if(path.startswith('http')):
		response = urlopen(path)
		image_data = response.read()
		image_data = BytesIO(image_data)
		image = Image.open(image_data)
	else:
		image_data = tf.io.gfile.GFile(path, 'rb').read()
		image = Image.open(BytesIO(image_data))

	(im_width, im_height) = image.size
	return np.array(image.getdata()).reshape(
		(1, im_height, im_width, 3)).astype(np.uint8)


COCO17_HUMAN_POSE_KEYPOINTS = [(0, 1),
	(0, 2),
	(1, 3),
	(2, 4),
	(0, 5),
	(0, 6),
	(5, 7),
	(7, 9),
	(6, 8),
	(8, 10),
	(5, 6),
	(5, 11),
	(6, 12),
	(11, 12),
	(11, 13),
	(13, 15),
	(12, 14),
	(14, 16)]


# ### Load label map data (for plotting).
# 
# Label maps correspond index numbers to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine.
# 
# We are going, for simplicity, to load from the repository that we loaded the Object Detection API code

def load_label_map_data():
	PATH_TO_LABELS = './models/research/object_detection/data/mscoco_label_map.pbtxt'
	category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
	return category_index


# ## Build a detection model and load pre-trained model weights

# ## Loading an image
#
# **Be careful:** when using images with an alpha channel, the model expect 3 channels images and the alpha will count as a 4th. 

def load_image(imgURL):
	flip_image_horizontally = False #@param {type:"boolean"}
	convert_image_to_grayscale = False #@param {type:"boolean"}

	try:
		image_np = load_image_into_numpy_array(imgURL)
	except:
		return None

	# Flip horizontally
	if(flip_image_horizontally):
		image_np[0] = np.fliplr(image_np[0]).copy()

	# Convert image to grayscale
	if(convert_image_to_grayscale):
		image_np[0] = np.tile(
			np.mean(image_np[0], 2, keepdims=True), (1, 1, 3)).astype(np.uint8)

	plt.figure(figsize=(24,32))

	# running inference
	model = ModelFactory().get_instance()
	hub_model = model.getModel()
	results = hub_model(image_np)

	# different object detection models have additional results
	# all of them are explained in the documentation
	result = {key:value.numpy() for key,value in results.items()}


	# ## Visualizing the results
	# 
	# Here is where we will need the TensorFlow Object Detection API to show the squares from the inference step (and the keypoints when available).

	label_id_offset = 0
	image_np_with_detections = image_np.copy()

	# Use keypoints if available in detections
	keypoints, keypoint_scores = None, None
	if 'detection_keypoints' in result:
		keypoints = result['detection_keypoints'][0]
		keypoint_scores = result['detection_keypoint_scores'][0]

	category_index = load_label_map_data()
	viz_utils.visualize_boxes_and_labels_on_image_array(
			image_np_with_detections[0],
			result['detection_boxes'][0],
			(result['detection_classes'][0] + label_id_offset).astype(int),
			result['detection_scores'][0],
			category_index,
			use_normalized_coordinates=True,
			max_boxes_to_draw=200,
			min_score_thresh=.30,
			agnostic_mode=False,
			keypoints=keypoints,
			keypoint_scores=keypoint_scores,
			keypoint_edges=COCO17_HUMAN_POSE_KEYPOINTS)

	plt.figure(figsize=(24,32))
	return calculTags(result['detection_scores'][0], (result['detection_classes'][0] + label_id_offset).astype(int), category_index)

def calculTags(detection_scores, detection_labels_id, category_index):
	tags = []
	for index in range(len(detection_scores)):
		score = detection_scores[index]
		if (score > 0.6):
			tags.append(category_index[detection_labels_id[index]]['name'])
	tags = list(dict.fromkeys(tags))
	if (len(tags) > 3):
		tags = tags[:-(len(tags)-3)]
	return tags